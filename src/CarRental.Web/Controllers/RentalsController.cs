using System.Threading.Tasks;
using CarRental.ApiModels;
using CarRental.Contracts;
using CarRental.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace CarRental.Controllers
{
    [Route("api/[controller]")]
    public class RentalsController : Controller
    {
        private readonly IRentalService _rentalService;

        public RentalsController(IRentalService rentalService)
        {
            _rentalService = rentalService;
        }

        [HttpPost("start")]
        public async Task<IActionResult> RegisterRentalStart([FromBody]CarRentalStartRequest body)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelValidationException("Model validation failed", ModelState);
            }

            CarRentalStartResponse response = await _rentalService.StartRental(body);
            
            return Ok(response);
        }
        
        [HttpPost("end")]
        public async Task<IActionResult> RegisterRentalStart([FromBody]CarRentalEndRequest body)
        {
            if (!ModelState.IsValid)
            {
                throw new ModelValidationException("Model validation failed", ModelState);
            }

            CarRentalEndResponse response = await _rentalService.EndRental(body);
            
            return Ok(response);
        }
    }
}

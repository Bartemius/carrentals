using CarRental.ApiModels;
using FluentValidation;

namespace CarRental.Validators
{
    public class CarRentalEndRequestValidator: AbstractValidator<CarRentalEndRequest>
    {
        public CarRentalEndRequestValidator()
        {
            RuleFor(x => x.BookingNumber)
                .NotNull()
                .Matches(@"BN_[0-9A-F]{8}");

            RuleFor(x => x.MileageKm)
                .InclusiveBetween(0, 999999);
        }
    }
}
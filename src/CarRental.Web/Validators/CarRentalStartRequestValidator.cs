using CarRental.ApiModels;
using FluentValidation;

namespace CarRental.Validators
{
    public class CarRentalStartRequestValidator: AbstractValidator<CarRentalStartRequest>
    {
        public CarRentalStartRequestValidator()
        {
            // <summary>
            // 3 letters, 2 digits, 1 alphanumeric
            // Excluded letters: I, Q, V, Å, Ä och Ö.
            // </summary>
            RuleFor(x => x.CarRegistrationNumber)
                .NotNull()
                .Matches(@"[ABCDEFGHJKLMNOPRSTUQXYZ]{3}\d{2}[0-9ABCDEFGHJKLMNOPRSTUQXYZ]");

            RuleFor(x => x.Personnummer)
                .NotNull()
                .Length(10)
                .Custom((personnummer, context) =>
                {
                    if (!Personnummer.Personnummer.Valid(personnummer))
                    {
                        context.AddFailure("Personnummer format is incorrect");
                    }
                });

            RuleFor(x => x.CarCategory)
                .NotNull();

            RuleFor(x => x.MileageKm)
                .InclusiveBetween(0, 999999);
        }
    }
}
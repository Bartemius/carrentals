namespace CarRental.Configuration
{
    public class PricingSettings
    {
        public decimal BaseDayPrice { get; set; }
        public decimal BaseKmPrice { get; set; }
    }
}
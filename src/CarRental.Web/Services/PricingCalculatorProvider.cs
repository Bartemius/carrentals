using System.Collections.Generic;
using CarRental.Configuration;
using CarRental.Contracts;
using CarRental.Entities;
using CarRental.Exceptions;
using CarRental.Pricing.Calculators;
using CarRental.Pricing.Contracts;

namespace CarRental.Services
{
    public class PricingCalculatorProvider: IPricingCalculatorProvider
    {
        private readonly Dictionary<CarCategory, IPricingCalculator> _calculators;

        public PricingCalculatorProvider(PricingSettings pricingSettings)
        {
            _calculators = new Dictionary<CarCategory, IPricingCalculator>
            {
                {CarCategory.Compact, new CompactCarPricingCalculator(pricingSettings.BaseDayPrice)},
                {CarCategory.Van, new VanPricingCalculator(pricingSettings.BaseDayPrice, pricingSettings.BaseKmPrice)},
                {CarCategory.Truck, new TruckPricingCalculator(pricingSettings.BaseDayPrice, pricingSettings.BaseKmPrice)},
            };
        }

        public IPricingCalculator GetPricingCalculator(CarCategory carCategory)
        {
            if (!_calculators.ContainsKey(carCategory))
                throw new CarRentalException(500, ErrorCodes.CarCategoryNotSupported, $"Can't calculate price for the car category {carCategory}");
            
            return _calculators[carCategory];
        }
    }
}
using System;
using System.Threading.Tasks;
using CarRental.ApiModels;
using CarRental.Contracts;
using CarRental.Entities;
using CarRental.Exceptions;
using CarRental.Helpers;
using CarRental.Pricing.Contracts;
using CarRental.Repositories.Contracts;

namespace CarRental.Services
{
    public class RentalService: IRentalService
    {
        private readonly IRentalRepository _repository;
        private readonly IPricingCalculatorProvider _pricingCalculatorProvider;

        public RentalService(IRentalRepository repository, IPricingCalculatorProvider pricingCalculatorProvider)
        {
            _repository = repository;
            _pricingCalculatorProvider = pricingCalculatorProvider;
        }

        public async Task<CarRentalStartResponse> StartRental(CarRentalStartRequest request)
        {
            bool isAlreadyRenting =
                await _repository.IsPersonAlreadyRentingACar(request.Personnummer, DateTime.UtcNow);
            if (isAlreadyRenting) 
                throw new CarRentalException(400, ErrorCodes.PersonHasActiveRental, "This person is already renting a car at this moment");
            
            bool isCarRented =
                await _repository.IsCarAlreadyRented(request.CarRegistrationNumber, DateTime.UtcNow);
            if (isCarRented) 
                throw new CarRentalException(400, ErrorCodes.CarHasActiveRental, "This car is already rented at this moment");
            
            Rental rental = new Rental()
            {
                CarRegistrationNumber = request.CarRegistrationNumber,
                Personnummer = request.Personnummer,
                CarCategory = Mapper.Map(request.CarCategory),
                RentalStartTime = DateTime.UtcNow,
                RentalEndTime = null,
                MileageStartKm = request.MileageKm,
                MileageEndKm = null,
                Price = 0
            };
            
            rental = await _repository.Insert(rental);
            return new CarRentalStartResponse() {
                BookingNumber = rental.BookingNumber
            };
        }

        public async Task<CarRentalEndResponse> EndRental(CarRentalEndRequest request)
        {
            var rental = await _repository.Get(request.BookingNumber);
            if (rental == null)
                throw new CarRentalException(400, ErrorCodes.NotFound, $"Booking {request.BookingNumber} not found.");
            
            if (rental.RentalEndTime != null)
                throw new CarRentalException(400, ErrorCodes.RentalHasEnded, "This rental has already ended.");

            if (rental.MileageStartKm > request.MileageKm)
                throw new CarRentalException(400, ErrorCodes.InvalidMileage, "Wrong mileage reported.");

            rental.RentalEndTime = DateTime.UtcNow;
            rental.MileageEndKm = request.MileageKm;
            
            CarUsage usage = new CarUsage()
            {
                TimeSpan = rental.RentalEndTime.Value - rental.RentalStartTime,
                DistanceKm = rental.MileageEndKm.Value - rental.MileageStartKm
            };

            var pricingEngine = _pricingCalculatorProvider.GetPricingCalculator(rental.CarCategory);
            rental.Price = pricingEngine.Calculate(usage);

            await _repository.Update(rental);
            
            return new CarRentalEndResponse()
            {
                Price = rental.Price,
                Currency = "SEK"
            };
        }
    }
}
namespace CarRental.Exceptions
{
    public static class ErrorCodes
    {
        public const string General = "General";
        public const string ModelValidationError = "ModelValidationError";
        public const string NotFound = "NotFound";
        
        
        public const string PersonHasActiveRental = "PersonHasActiveRental";
        public const string CarHasActiveRental = "CarHasActiveRental";
        public const string RentalHasEnded = "RentalHasEnded";
        public const string InvalidMileage = "InvalidMileage";
        public const string CarCategoryNotSupported = "CarCategoryNotSupported";
    }
}
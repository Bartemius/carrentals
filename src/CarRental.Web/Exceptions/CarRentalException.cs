using System;
using System.Runtime.Serialization;

namespace CarRental.Exceptions
{
    public class CarRentalException: Exception
    {
        public int StatusCode { get; }
        public string ErrorCode { get; }

        public CarRentalException(int statusCode, string errorCode, string message) : base(message)
        {
            StatusCode = statusCode;
            ErrorCode = errorCode;
        }

        public CarRentalException(int statusCode, string errorCode, string message, Exception innerException) 
            : base(message, innerException)
        {
            StatusCode = statusCode;
            ErrorCode = errorCode;
        }
    }
}
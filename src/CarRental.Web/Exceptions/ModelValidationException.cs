using System;
using System.Runtime.Serialization;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace CarRental.Exceptions
{
    public class ModelValidationException: Exception
    {
        public ModelStateDictionary ModelState { get; }

        public ModelValidationException(string message, ModelStateDictionary modelState) : base(message)
        {
            ModelState = modelState;
        }
    }
}
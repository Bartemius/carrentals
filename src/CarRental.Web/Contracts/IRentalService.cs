using System.Threading.Tasks;
using CarRental.ApiModels;

namespace CarRental.Contracts
{
    public interface IRentalService
    {
        Task<CarRentalStartResponse> StartRental(CarRentalStartRequest request);
        Task<CarRentalEndResponse> EndRental(CarRentalEndRequest body);
    }
}
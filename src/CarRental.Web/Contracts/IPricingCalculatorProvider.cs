using CarRental.Entities;
using CarRental.Pricing.Contracts;

namespace CarRental.Contracts
{
    public interface IPricingCalculatorProvider
    {
        IPricingCalculator GetPricingCalculator(CarCategory carCategory);
    }
}
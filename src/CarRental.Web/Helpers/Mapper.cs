using System;
using CarRental.Exceptions;
using CarCategoryEntity = CarRental.Entities.CarCategory;

namespace CarRental.Helpers
{
    public static class Mapper
    {
        public static CarCategoryEntity Map(string category)
        {
            if (Enum.TryParse<CarCategoryEntity>(category, out var carCategory))
                return carCategory;
            
            throw new CarRentalException(400, ErrorCodes.CarCategoryNotSupported, $"Unsupported car category: {category}");
        }
    }
}
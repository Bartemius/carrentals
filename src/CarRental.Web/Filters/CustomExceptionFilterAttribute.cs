using System.Collections.Generic;
using System.Linq;
using CarRental.ApiModels;
using CarRental.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;

namespace CarRental.Filters
{
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly ILogger<CustomExceptionFilterAttribute> _logger;

        public CustomExceptionFilterAttribute(ILogger<CustomExceptionFilterAttribute> logger)
        {
            _logger = logger;
        }
        
        public override void OnException(ExceptionContext context)
        {
            _logger.LogError(context.Exception, $"Exception: {context.Exception.Message}");
            
            int httpStatusCode = 500;
            ErrorModel model = new ErrorModel()
            {
                ErrorCode = ErrorCodes.General,
                Message = context.Exception.Message,
                Data = null
            };

            if (context.Exception is CarRentalException carRentalException)
            {
                httpStatusCode = carRentalException.StatusCode;
                model.ErrorCode = carRentalException.ErrorCode;
            }
            else if (context.Exception is ModelValidationException modelValidationException)
            {
                httpStatusCode = 400;
                model.ErrorCode = ErrorCodes.ModelValidationError;
                model.Data = ConvertModelState(modelValidationException.ModelState);
            }
            
            context.Result = new ObjectResult(model) { StatusCode = httpStatusCode };
        }

        private IDictionary<string, string> ConvertModelState(ModelStateDictionary modelState)
        {
            Dictionary<string, string> dict  = new Dictionary<string, string>();
            foreach (var entry in modelState)
            {
                var errorMessages = entry.Value.Errors.Select(er => er.ErrorMessage);
                dict[entry.Key] = string.Join(" ", errorMessages);
            }

            return dict;
        }
    }}
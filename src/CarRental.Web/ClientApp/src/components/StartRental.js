import React, { Component } from 'react';

export class StartRental extends Component {
  static displayName = StartRental.name;

  constructor (props) {
    super(props);
    this.state = {
      CarRegistrationNumber: "",
      Personnummer: "",
      CarCategory: "Compact",
      MileageKm: "",
      
      loading: false,
      showResponse: false,
      response: null
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    this.setState({ loading: true, showResponse: false });
    let that = this;

    fetch('/api/rentals/start', {
          method: 'post',
          headers: {'Content-Type':'application/json'},
          body: JSON.stringify({
            "CarRegistrationNumber": this.state.CarRegistrationNumber,
            "Personnummer": this.state.Personnummer,
            "CarCategory": this.state.CarCategory,
            "MileageKm": this.state.MileageKm,
          })
        })
        .then(response => {
          if (response.ok) {
            return response.json();
          }
          throw response;
        })
        .then(response => {
            that.setState({ response: response, loading: false, showResponse: true, success: true });
        })
        .catch((error) => {
            error.json().then(body => {
              that.setState({ response: body, loading: false, showResponse: true, success: false });
            });
        });
  }
  
  render () {
     let loading = this.state.loading
         ? <div>Loading...</div>
         : '';
         
    let response = '';
    if (this.state.showResponse) {
      if(this.state.success) {
        response = <div>Booking number: <span><strong>{this.state.response.bookingNumber}</strong></span></div>;
      } else {
        response = <div>{this.state.response.message}</div>;
        if (this.state.response.data) {
          var data = this.state.response.data;
          response =
              <div>{this.state.response.message}    
                <ul>
                  <li>{data.CarRegistrationNumber}</li>
                  <li>{data.Personnummer}</li>
                  <li>{data.CarCategory}</li>
                  <li>{data.MileageKm}</li>
                </ul>
              </div>;
        }
      }
    }
    
    return (
      <div>
        <h1>Start rental</h1>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="carRegistrationNumber">Car Registration Number</label>
            <input type="text" className="form-control" id="carRegistrationNumber" placeholder="AAA00A"
                   value={this.state.CarRegistrationNumber}
                   onChange={e => this.setState({CarRegistrationNumber: e.target.value})}/>
      
          </div>
          <div className="form-group">
            <label htmlFor="personnummer">Personnummer</label>
            <input type="text" className="form-control" id="personnummer" placeholder="9001011234"
                   value={this.state.Personnummer}
                   onChange={e => this.setState({Personnummer: e.target.value})}/>
          </div>
          <div className="form-group">
            <label htmlFor="carCategory">Car Category</label>
            <select className="form-control" id="carCategory"
                    value={this.state.CarCategory}
                    onChange={e => this.setState({CarCategory: e.target.value})}>
              <option value="Compact">Compact car</option>
              <option value="Van">Van</option>
              <option value="Truck">Truck</option>
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="mileageKm">Milieage, km</label>
            <input type="number" className="form-control" id="mileageKm" placeholder="12600"
                   value={this.state.MileageKm}
                   onChange={e => this.setState({MileageKm: e.target.value})}/>
          </div>
          <button type="submit" className="btn btn-primary">Start Rental</button>
        </form>

        {loading}
        
        {response}
      </div>
    );
  }
}

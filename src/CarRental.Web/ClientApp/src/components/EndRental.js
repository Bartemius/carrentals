import React, { Component } from 'react';

export class EndRental extends Component {
  static displayName = EndRental.name;

    constructor (props) {
        super(props);
        this.state = {
            BookingNumber: "",
            MileageKm: "",

            loading: false,
            showResponse: false,
            response: null
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({ loading: true, showResponse: false });
        let that = this;

        fetch('/api/rentals/end', {
            method: 'post',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({
                "BookingNumber": this.state.BookingNumber,
                "MileageKm": this.state.MileageKm,
            })
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(response => {
                that.setState({ response: response, loading: false, showResponse: true, success: true });
            })
            .catch((error) => {
                error.json().then(body => {
                    that.setState({ response: body, loading: false, showResponse: true, success: false });
                });
            });
    }

    render () {
        let loading = this.state.loading
            ? <div>Loading...</div>
            : '';

        let response = '';
        if (this.state.showResponse) {
            if(this.state.success) {
                response = <div>Your price: <span><strong>{this.state.response.price} {this.state.response.currency}</strong></span></div>;
            } else {
                response = <div>{this.state.response.message}</div>;
                if (this.state.response.data) {
                    var data = this.state.response.data;
                    response =
                        <div>{this.state.response.message}
                            <ul>
                                <li>{data.BookingNumber}</li>
                                <li>{data.MileageKm}</li>
                            </ul>
                        </div>;
                }
            }
        }

        return (
            <div>
                <h1>Start rental</h1>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="bookingNumber">Booking Number</label>
                        <input type="text" className="form-control" id="bookingNumber" placeholder="BN_D67195B5"
                               value={this.state.BookingNumber}
                               onChange={e => this.setState({BookingNumber: e.target.value})}/>

                    </div>
                    <div className="form-group">
                        <label htmlFor="mileageKm">Milieage, km</label>
                        <input type="number" className="form-control" id="mileageKm" placeholder="12600"
                               value={this.state.MileageKm}
                               onChange={e => this.setState({MileageKm: e.target.value})}/>
                    </div>
                    <button type="submit" className="btn btn-primary">Finish Rental</button>
                </form>

                {loading}

                {response}
            </div>
        );
    }
}

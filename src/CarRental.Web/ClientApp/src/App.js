import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { StartRental } from './components/StartRental';
import { EndRental } from './components/EndRental';

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={StartRental} />
        <Route path='/startrental' component={StartRental} />
        <Route path='/endrental' component={EndRental} />
      </Layout>
    );
  }
}

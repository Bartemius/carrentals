﻿using System;
using System.Threading.Tasks;
using CarRental.Entities;

namespace CarRental.Repositories.Contracts
{
    public interface IRentalRepository
    {
        Task<bool> IsPersonAlreadyRentingACar(string personnummer, DateTime time);
        Task<bool> IsCarAlreadyRented(string registrationNumber, DateTime time);
        Task<Rental> Get(string requestBookingNumber);
        Task<Rental> Insert(Rental rental);
        Task Update(Rental rental);
    }
}
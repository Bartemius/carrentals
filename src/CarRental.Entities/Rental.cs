﻿using System;

namespace CarRental.Entities
{
    public class Rental
    {
        public string BookingNumber { get; set; }
        
        public string CarRegistrationNumber { get; set; }
        
        public string Personnummer { get; set; }
        
        public CarCategory CarCategory { get; set; }

        public DateTime RentalStartTime { get; set; }
        
        public DateTime? RentalEndTime { get; set; }
        
        public decimal Price { get; set; }
        
        public int MileageStartKm { get; set; }
        public int? MileageEndKm { get; set; }
    }
}
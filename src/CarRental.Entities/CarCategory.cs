namespace CarRental.Entities
{
    public enum CarCategory
    {
        Compact,
        Van,
        Truck
    }
}
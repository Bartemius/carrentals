﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarRental.Entities;
using CarRental.Repositories.Contracts;

namespace CarRental.Repositories.InMemory
{
    public class RentalRepository: IRentalRepository
    {
        private IList<Rental> _list;

        public RentalRepository()
        {
            _list = new List<Rental> { };
        }
        
        public Task<bool> IsPersonAlreadyRentingACar(string personnummer, DateTime time)
        {
            bool isAlreadyRenting = _list.Any(rental =>
                rental.Personnummer == personnummer
                && rental.RentalStartTime < time
                && !rental.RentalEndTime.HasValue);

            return Task.FromResult(isAlreadyRenting);
        }

        public Task<bool> IsCarAlreadyRented(string registrationNumber, DateTime time)
        {
            bool isAlreadyRented = _list.Any(rental =>
                rental.CarRegistrationNumber == registrationNumber
                && rental.RentalStartTime < time
                && !rental.RentalEndTime.HasValue);

            return Task.FromResult(isAlreadyRented);
        }

        public Task<Rental> Get(string requestBookingNumber)
        {
            Rental rental = _list.FirstOrDefault(r => r.BookingNumber == requestBookingNumber);
            return Task.FromResult(rental);
        }

        public Task<Rental> Insert(Rental rental)
        {
            if (rental.BookingNumber != null)
                throw new ArgumentException("Booking number cannot be defined on a new booking");

            rental.BookingNumber = BookingNumberGenerator.Generate();
            _list.Add(rental);

            return Task.FromResult(rental);
        }

        public async Task Update(Rental rental)
        {
            var existing = await Get(rental.BookingNumber);
            _list.Remove(existing);
            
            _list.Add(rental);
        }
    }
}
using System;

namespace CarRental.Repositories.InMemory
{
    public static class BookingNumberGenerator
    {
        public static string Generate()
        {
            //return $"BN_{DateTime.UtcNow:yyMMddHHmmss}";
            return $"BN_{Guid.NewGuid().ToString().Substring(0, 8).ToUpper()}";
        }
    }
}
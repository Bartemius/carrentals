namespace CarRental.ApiModels
{
    public class CarRentalStartResponse
    {
        public string BookingNumber { get; set; }
    }
}
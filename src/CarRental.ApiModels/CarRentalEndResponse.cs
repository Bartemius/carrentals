namespace CarRental.ApiModels
{
    public class CarRentalEndResponse
    {
        public decimal Price { get; set; }
        public string Currency { get; set; }
    }
}
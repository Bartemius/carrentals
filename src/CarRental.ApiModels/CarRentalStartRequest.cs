﻿namespace CarRental.ApiModels
{
    public class CarRentalStartRequest
    {
        public string CarRegistrationNumber { get; set; }
        
        public string Personnummer { get; set; }
        
        public string CarCategory { get; set; }
        
        public int MileageKm { get; set; }
    }
}

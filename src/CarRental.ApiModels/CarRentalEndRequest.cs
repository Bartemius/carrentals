﻿namespace CarRental.ApiModels
{
    public class CarRentalEndRequest
    {
        public string BookingNumber { get; set; }
        
        public int MileageKm { get; set; }
    }
}

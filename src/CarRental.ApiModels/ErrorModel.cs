namespace CarRental.ApiModels
{
    public class ErrorModel
    {
        public string ErrorCode { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
using System;
using CarRental.Pricing.Contracts;

namespace CarRental.Pricing.Calculators
{
    public class VanPricingCalculator: IPricingCalculator
    {
        private const decimal DayPriceModifier = 1.3m;

        private readonly decimal _dayPrice;
        private readonly decimal _kmPrice;

        public VanPricingCalculator(decimal dayPrice, decimal kmPrice)
        {
            _dayPrice = dayPrice;
            _kmPrice = kmPrice;
        }

        public decimal Calculate(CarUsage carUsage)
        {
            if (carUsage.TimeSpan.TotalDays < 0)
                throw new ArgumentException($"Car usage time span can not be below zero.");
            
            if (carUsage.DistanceKm < 0)
                throw new ArgumentException($"Car usage distance can not be below zero km.");
            
            var days = (decimal)Math.Ceiling(carUsage.TimeSpan.TotalDays);

            return _dayPrice * days * DayPriceModifier + _kmPrice * carUsage.DistanceKm;
        }
    }
}
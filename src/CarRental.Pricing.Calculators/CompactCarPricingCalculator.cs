using System;
using CarRental.Pricing.Contracts;

namespace CarRental.Pricing.Calculators
{
    public class CompactCarPricingCalculator: IPricingCalculator
    {
        private readonly decimal _dayPrice;

        public CompactCarPricingCalculator(decimal dayPrice)
        {
            _dayPrice = dayPrice;
        }

        public decimal Calculate(CarUsage carUsage)
        {
            if (carUsage.TimeSpan.TotalDays < 0)
                throw new ArgumentException($"Car usage time span can not be below zero.");
            
            var days = (decimal)Math.Ceiling(carUsage.TimeSpan.TotalDays);

            return _dayPrice * days;
        }
    }
}
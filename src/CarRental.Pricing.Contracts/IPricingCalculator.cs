﻿using System;

namespace CarRental.Pricing.Contracts
{
    public interface IPricingCalculator
    {
        decimal Calculate(CarUsage carUsage);
    }
}
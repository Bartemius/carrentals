using System;

namespace CarRental.Pricing.Contracts
{
    public class CarUsage
    {
        public CarUsage()
        {
        }

        public CarUsage(TimeSpan timeSpan, int distanceKm)
        {
            TimeSpan = timeSpan;
            DistanceKm = distanceKm;
        }

        public TimeSpan TimeSpan { get; set; }
        public int DistanceKm { get; set; }
    }
}
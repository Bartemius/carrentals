using System;
using System.Threading.Tasks;
using CarRental.ApiModels;
using CarRental.Configuration;
using CarRental.Exceptions;
using CarRental.Repositories.InMemory;
using CarRental.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarRental.Pricing.UnitTests
{
    [TestClass]
    public class ServiceTests
    {
        private RentalService _service;

        [TestInitialize]
        public void Init()
        {
            var repository = new RentalRepository();
            var pricingCalculatorProvider = new PricingCalculatorProvider(new PricingSettings()
            {
                BaseDayPrice = 100,
                BaseKmPrice = 1
            });
            _service = new RentalService(repository, pricingCalculatorProvider);
        }

        [TestMethod]
        public async Task HappyFlowTest()
        {
            // Start request
            var rentalRequest = new CarRentalStartRequest()
            {
                Personnummer = "9710201642",
                CarRegistrationNumber = "AAA000",
                CarCategory = "Compact",
                MileageKm = 1000
            };
            var rentalStartResult = await _service.StartRental(rentalRequest);

            Assert.IsTrue(rentalStartResult.BookingNumber.StartsWith("BN_"));
            
            // End request
            var rentalEndRequest = new CarRentalEndRequest()
            {
                BookingNumber = rentalStartResult.BookingNumber,
                MileageKm = 1200
            };
            var rentalEndResult = await _service.EndRental(rentalEndRequest);
            
            Assert.AreEqual(100m, rentalEndResult.Price);
            Assert.AreEqual("SEK", rentalEndResult.Currency);
        }

        [TestMethod]
        public async Task PersonAlreadyHasActiveRentalTest()
        {
            // Start request
            var rentalRequest = new CarRentalStartRequest()
            {
                Personnummer = "9710201642",
                CarRegistrationNumber = "AAA000",
                CarCategory = "Compact",
                MileageKm = 1000
            };
            var rentalStartResult = await _service.StartRental(rentalRequest);

            Assert.IsTrue(rentalStartResult.BookingNumber.StartsWith("BN_"));
            
            
            // One more Start request
            var rentalRequest2 = new CarRentalStartRequest()
            {
                Personnummer = "9710201642",
                CarRegistrationNumber = "AAA001",
                CarCategory = "Compact",
                MileageKm = 1000
            };
            try
            {
                var rentalStartResult2 = await _service.StartRental(rentalRequest2);
                Assert.Fail("Previous line should throw an exception");
            }
            catch (CarRentalException ex)
            {
                Assert.AreEqual(ErrorCodes.PersonHasActiveRental, ex.ErrorCode);
            }
            catch (Exception ex)
            {
                Assert.Fail("This is not the exception we're looking for");
            }
        }
        
        [TestMethod]
        public async Task TheCarHasActiveRentalTest()
        {
            // Start request
            var rentalRequest = new CarRentalStartRequest()
            {
                Personnummer = "9710201642",
                CarRegistrationNumber = "AAA000",
                CarCategory = "Compact",
                MileageKm = 1000
            };
            var rentalStartResult = await _service.StartRental(rentalRequest);

            Assert.IsTrue(rentalStartResult.BookingNumber.StartsWith("BN_"));
            
            
            // One more Start request
            var rentalRequest2 = new CarRentalStartRequest()
            {
                Personnummer = "7510022937",
                CarRegistrationNumber = "AAA000",
                CarCategory = "Compact",
                MileageKm = 1000
            };
            try
            {
                var rentalStartResult2 = await _service.StartRental(rentalRequest2);
                Assert.Fail("Previous line should throw an exception");
            }
            catch (CarRentalException ex)
            {
                Assert.AreEqual(ErrorCodes.CarHasActiveRental, ex.ErrorCode);
            }
            catch (Exception ex)
            {
                Assert.Fail("This is not the exception we're looking for");
            }
        }
        
        [TestMethod]
        public async Task BookingNotFound()
        {
            var rentalEndRequest = new CarRentalEndRequest()
            {
                BookingNumber = "BN_00001111",
                MileageKm = 1200
            };
            
            try
            {
                var rentalEndResult = await _service.EndRental(rentalEndRequest);
                Assert.Fail("Previous line should throw an exception");
            }
            catch (CarRentalException ex)
            {
                Assert.AreEqual(ErrorCodes.NotFound, ex.ErrorCode);
            }
            catch (Exception ex)
            {
                Assert.Fail("This is not the exception we're looking for");
            }
        }
        
        [TestMethod]
        public async Task IncorrectMileageReportedTest()
        {
            // Start request
            var rentalRequest = new CarRentalStartRequest()
            {
                Personnummer = "9710201642",
                CarRegistrationNumber = "AAA000",
                CarCategory = "Compact",
                MileageKm = 1000
            };
            var rentalStartResult = await _service.StartRental(rentalRequest);

            Assert.IsTrue(rentalStartResult.BookingNumber.StartsWith("BN_"));
            
            
            // One more Start request
            var rentalEndRequest = new CarRentalEndRequest()
            {
                BookingNumber = rentalStartResult.BookingNumber,
                MileageKm = 900
            };
            
            try
            {
                var rentalEndResult = await _service.EndRental(rentalEndRequest);
                Assert.Fail("Previous line should throw an exception");
            }
            catch (CarRentalException ex)
            {
                Assert.AreEqual(ErrorCodes.InvalidMileage, ex.ErrorCode);
            }
            catch (Exception ex)
            {
                Assert.Fail("This is not the exception we're looking for");
            }
        }
        
        [TestMethod]
        public async Task EndingRentalTwiceTest()
        {
            // Start request
            var rentalRequest = new CarRentalStartRequest()
            {
                Personnummer = "9710201642",
                CarRegistrationNumber = "AAA000",
                CarCategory = "Compact",
                MileageKm = 1000
            };
            var rentalStartResult = await _service.StartRental(rentalRequest);

            Assert.IsTrue(rentalStartResult.BookingNumber.StartsWith("BN_"));
            
            // End request
            var rentalEndRequest = new CarRentalEndRequest()
            {
                BookingNumber = rentalStartResult.BookingNumber,
                MileageKm = 1200
            };
            var rentalEndResult1 = await _service.EndRental(rentalEndRequest);
            
            Assert.AreEqual(100m, rentalEndResult1.Price);
            Assert.AreEqual("SEK", rentalEndResult1.Currency);

            
            try
            {
                // Trying to end the same Rental again
                var rentalEndResult2 = await _service.EndRental(rentalEndRequest);
                Assert.Fail("Previous line should throw an exception");
            }
            catch (CarRentalException ex)
            {
                Assert.AreEqual(ErrorCodes.RentalHasEnded, ex.ErrorCode);
            }
            catch (Exception ex)
            {
                Assert.Fail("This is not the exception we're looking for");
            }
        }
    }
}
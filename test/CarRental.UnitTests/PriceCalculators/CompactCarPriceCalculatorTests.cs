using System;
using CarRental.Pricing.Calculators;
using CarRental.Pricing.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarRental.Pricing.UnitTests.PriceCalculators
{
    [TestClass]
    public class CompactCarPriceCalculatorTests
    {
        private IPricingCalculator _calculator;

        [TestInitialize]
        public void Init()
        {
            _calculator = new CompactCarPricingCalculator(100);
        }
        
        [TestMethod]
        public void HalfDayUseTest()
        {
            // rounds to 1 day
            CarUsage usage = new CarUsage(TimeSpan.FromHours(12), 0);

            var price = _calculator.Calculate(usage);
            
            Assert.AreEqual(100, price);
        }
        
        [TestMethod]
        public void OneDayUseTest()
        {
            // 1 day
            CarUsage usage = new CarUsage(TimeSpan.FromHours(24), 0);

            var price = _calculator.Calculate(usage);
            
            Assert.AreEqual(100, price);
        }
        
        [TestMethod]
        public void FewDaysUseTest()
        {
            // 6 days
            CarUsage usage = new CarUsage(new TimeSpan(5, 6, 23, 37), 0);

            var price = _calculator.Calculate(usage);
            
            Assert.AreEqual(600, price);
        }
        
        [TestMethod]
        public void ZeroTimeTest()
        {
            CarUsage usage = new CarUsage(TimeSpan.Zero, 0);

            var price = _calculator.Calculate(usage);
            
            Assert.AreEqual(0, price);
        }
        
        [TestMethod]
        public void NegativeTimeTest()
        {
            var period = DateTime.Now - DateTime.Now.AddDays(1);
            CarUsage usage = new CarUsage(period, 0);
            
            Assert.ThrowsException<ArgumentException>(() => _calculator.Calculate(usage));
        }
    }
}
using System;
using CarRental.Pricing.Calculators;
using CarRental.Pricing.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarRental.Pricing.UnitTests.PriceCalculators
{
    [TestClass]
    public class TruckPriceCalculatorTests
    {
        private IPricingCalculator _calculator;

        [TestInitialize]
        public void Init()
        {
            _calculator = new TruckPricingCalculator(100m, 1m);
        }
        
        [TestMethod]
        public void NormalUseTest()
        {
            // 1 day, 18 km
            CarUsage usage = new CarUsage(TimeSpan.FromHours(12), 18);

            var price = _calculator.Calculate(usage);
            
            Assert.AreEqual(177m, price);
        }

        [TestMethod]
        public void FewDaysUseTest()
        {
            // 6 days, 736 km
            CarUsage usage = new CarUsage(new TimeSpan(5, 6, 23, 37), 736);

            var price = _calculator.Calculate(usage);
            
            Assert.AreEqual(2004, price);
        }
        
        [TestMethod]
        public void ZeroTimeAndDistanceTest()
        {
            // Allowed corner case
            CarUsage usage = new CarUsage(TimeSpan.Zero, 0);

            var price = _calculator.Calculate(usage);
            
            Assert.AreEqual(0, price);
        }
        
        [TestMethod]
        public void NegativeTimeTest()
        {
            var period = DateTime.Now - DateTime.Now.AddDays(1);
            CarUsage usage = new CarUsage(period, 15);
            
            Assert.ThrowsException<ArgumentException>(() => _calculator.Calculate(usage));
        }
        
        [TestMethod]
        public void NegativeDistanceTest()
        {
            CarUsage usage = new CarUsage(TimeSpan.FromDays(1), -15);
            
            Assert.ThrowsException<ArgumentException>(() => _calculator.Calculate(usage));
        }
    }
}